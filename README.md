# Ludum Dare 43

**Sacrifices Must Be Made**

[From Dictionary.com:](https://www.dictionary.com/browse/sacrifice) _sacrifice_
-- the surrender or destruction of something prized or desirable for the sake of
something considered as having a higher or more pressing claim.

## Brainstorming

### Personal Goals

- Continue to develop skills with Piston2D and spend less time on boilerplate
  this time around.

- Focus on game designs where levels can be automatically/procedurally
  generated so I can do more development and less design.

### Theme Inspiration

 As always, I want to apply the theme to the mechanics of the game, not just
the way that the game looks/feels. I want to use the concept of _sacrifice_ as
a crucial part of gameplay strategy.

 Sacrifice is a complex concept. You have to give something up now for an
advantage in the future, but it's even more than that. Take chess, for example.
In chess strategy, sacrifices can be classified as "real" or "sham." Real
sacrifices are much riskier, with less obvious future benefits, while sham
sacrifices have a much clearer (usually definite) future gain. Real sacrifices
involve chances and can require very precise play to maintain an advantage or
may rather require very precise play from the opponent for them to notice and
avoid any traps.

 At this point, I'm considering a puzzle/platformer game that requires
sacrifice to progress, and that sacrifice involves a choice that will affect
future capabilities. Most likely, the player will have a very limited or no
view of the future challenges, taking away the possibility of calculation and
creating more risk.

It will be important to balance the consequences of sacrifice with
fun and playability. Ideally, the consequences will make it more difficult to
proceed, requiring more thought or skill, but never make it impossible.

## The game

My initial idea is to create a platformer-style game, but instead of buffs,
there are nerfs. For example, in Mario games, the mushroom will increase your
size and give you more abilities, the fire flower will let you shoot fireballs.
In contrast, my game could require debuffs like reduced jump height, reduced
speed, or complete removal of less critical abilities.
