use graphics::{Graphics, Image};
use graphics::math::Matrix2d;
use piston::input::UpdateArgs;

use context::Context;

pub struct Entity {
    pub texture: Image,
    pub pos_x: f64,
    pub pos_y: f64,
    pub vel_x: f64,
    pub vel_y: f64,
}

impl Entity {
    pub fn update(&mut self, _ctx: &mut Context, args: &UpdateArgs) {
        self.pos_x += self.vel_x * args.dt;
        self.pos_y += self.vel_y * args.dt;
    }

    pub fn draw<G>(&mut self, graphics: &mut G, texture: &G::Texture, transform: Matrix2d)
    where
        G: Graphics,
    {
        use graphics::*;

        self.texture.draw(
            texture,
            &Default::default(), 
            transform.trans(self.pos_x, self.pos_y),
            graphics,
        );
    }
}
