/// The height of the screen measured by the number of visible tiles.
pub const VIEW_HEIGHT: f64 = 12.0;

/// The width of the screen measured by the number of visible tiles.
pub const VIEW_WIDTH: f64 = 16.0;

pub const FRAME_RATE: u64 = 60;
pub const UPDATE_RATE: u64 = 60;

pub const TILE_PIXELS: u32 = 8;
