use opengl_graphics::{GlGraphics, Texture};
use piston::event_loop::Events;
use piston::input::Event;
use sdl2_window::Sdl2Window;

pub struct Context {
    pub window: Sdl2Window,
    pub graphics: GlGraphics,
    pub events: Events,
    pub texture: Texture,
}

impl Context {
    pub fn next_event(&mut self) -> Option<Event> {
        self.events.next(&mut self.window)
    }
}
