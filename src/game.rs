use graphics::Image;
use piston::input::{Event, Input, Loop, RenderArgs, UpdateArgs};
use piston::window::{Size, Window};

use consts::{VIEW_HEIGHT, TILE_PIXELS};
use context::Context;
use entity::Entity;

fn tile(x: u32, y: u32, width: u32, height: u32) -> Image {
    Image::new()
        .rect([0.0, 0.0, width as f64, height as f64])
        .src_rect([
            (x * TILE_PIXELS) as f64,
            ((y + height) * TILE_PIXELS) as f64,
            (width * TILE_PIXELS) as f64,
            -((height * TILE_PIXELS) as f64),
        ])
}

pub struct Game {
    view_x: f64,
    view_y: f64,
    player: Entity,
}

impl Game {
    pub fn new() -> Game {
        Game {
            view_x: 0.0,
            view_y: 0.0,
            player: Entity {
                texture: tile(0, 0, 1, 1),
                pos_x: 0.0,
                pos_y: 0.0,
                vel_x: 1.0,
                vel_y: 1.0,
            },
        }
    }

    pub fn handle(&mut self, ctx: &mut Context, event: &Event) {
        match event {
            Event::Loop(Loop::Render(args)) => self.render(ctx, args),
            Event::Loop(Loop::Update(args)) => self.update(ctx, args),
            Event::Input(input) => self.input(ctx, input),
            _ => {}
        }
    }

    fn input(&mut self, _ctx: &mut Context, _input: &Input) {}

    fn update(&mut self, ctx: &mut Context, args: &UpdateArgs) {
        self.player.update(ctx, args);
    }

    fn render(&mut self, ctx: &mut Context, args: &RenderArgs) {
        let Size {
            width: _width,
            height,
        } = ctx.window.draw_size();
        let Context {
            graphics,
            texture,
            window: _,
            events: _,
        } = ctx;
        graphics.draw(args.viewport(), |context, graphics| {
            use graphics::*;
            let context = context
                .scale(
                    (height as f64) / VIEW_HEIGHT,
                    -(height as f64) / VIEW_HEIGHT,
                ).trans(-self.view_x, -self.view_y - VIEW_HEIGHT);

            clear([0.0, 0.0, 0.0, 1.0], graphics);
            self.player.draw(graphics, texture, context.transform);
        });
    }
}
