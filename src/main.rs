extern crate image;
extern crate graphics;
extern crate music;
extern crate opengl_graphics;
extern crate piston;
extern crate sdl2_window;

pub mod consts;
pub mod context;
pub mod entity;
pub mod game;

use consts::{FRAME_RATE, UPDATE_RATE};
use context::Context;
use game::Game;

use opengl_graphics::{Filter, GlGraphics, OpenGL, Texture, TextureSettings};
use piston::event_loop::{EventLoop, Events};
use piston::window::WindowSettings;
use sdl2_window::Sdl2Window;

const OPENGL_VERSION: OpenGL = OpenGL::V2_1;

fn main() {
    let mut context = Context {
        window: WindowSettings::new("LD43", (800, 600))
            .exit_on_esc(true)
            .build::<Sdl2Window>()
            .expect("Failed to create window"),

        events: Events::new(Default::default())
            .max_fps(FRAME_RATE)
            .ups(UPDATE_RATE)
            .swap_buffers(true),

        graphics: GlGraphics::new(OPENGL_VERSION),

        texture: Texture::from_image(
            &image::load_from_memory(include_bytes!("textures.png"))
                .expect("Cannot read textures")
                .to_rgba(),
            &TextureSettings::new()
                .filter(Filter::Nearest),
        ),
    };
    let mut game = Game::new();

    while let Some(event) = context.next_event() {
        game.handle(&mut context, &event)
    }
}
